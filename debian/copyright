Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sfsmisc
Upstream-Contact: Martin Maechler <maechler@stat.math.ethz.ch>
Source: https://cran.r-project.org/package=sfsmisc

Files: *
Copyright: 1992-2017 Martin Maechler,
                     Seminar for Statistics, ETH Zurich
License: GPL-2+

Files: man/inv.seq.Rd
Copyright: 1995 Martin Maechler, Tony Plate
License: GPL-2+

Files: R/misc-goodies.R
       man/p.hboxp.Rd
Copyright: 1995-1996 Christian Keller, Markus Keller, Martin Maechler
License: GPL-2+

Files: man/u.Datumvonheute.Rd
Copyright: 1999 Caterina Savi, Martin Maechler
License: GPL-2+

Files: man/pretty10exp.Rd
Copyright: 2000 Martin Maechler, Ben Bolker
License: GPL-2+

Files: man/f.robftest.Rd
Copyright: 2000 Werner Stahel, Martin Maechler
License: GPL-2+

Files: debian/*
Copyright: 2017 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License at /usr/share/common-licenses/GPL-2.
